# Postgre Sql Week 7
### Credit Card Number Card Holder IDCustome r Name Number of Transa Total
Charges###

\'501879657465 12 Megan Price 66 14,441.72

\'5570600642865850 16 Crystal Clark 71 14,368.56

\'3581345943543940 6 Beth Hernandez 53 14,089.77

\'4761049645711550000 1 Robert Johnson 65 14,081.58

\'344119623920892 18 Malik Carlson 68 11,509.64

\'4319653513507 25 Nancy Contreras 60 11,315.05

\'3516952396080240 7 Sean Taylor 61 10,686.92

\'30181963913340 9 Laurie Gibbs 61 10,391.27

\'30078299053512 3 Elizabeth Sawyer 58 8,071.99

\'30142966699187 24 Stephanie Dalton 57 7,272.62

1).The above 10 cards are showing higher amount of spending. The
number of the higher spending here is about 20%. But these are out of
a53 total card holders, that too is 20%. So, the higher amount of
spending is not a conclusive source to make any decision. Graphs
indicate cardholder 18 have many small payments, but the total number of
payments are too high for him either. None of the credit Cards are
showing higher counts of under \$2 spending either. If there is any
fraud this is not the way to isolate the possible candidates.

2).The data we see here is only for year 2018. We do not have the
previous years history to compare, if assumed there were no frauds in
prior years. So, analyzing the data by splitting the time into smaller
units can reveal clues. We can go for monthly comparison, weekly
comparison, or specific times of the day, like comparison like Morning,
evening & night.

Morning Spending

Time Date Card Number Card Holder Name Merchant Type Amount

7:22:03 12/7/2018 4761049645711550000 Robert Johnson bar \$1,894.00

8:26:08 3/5/2018 5570600642865850 Crystal Clark bar \$1,617.00

7:18:09 3/6/2018 4319653513507 Nancy Contreras bar \$1,334.00

8:07:03 1/22/2018 5570600642865850 Crystal Clark restaurant \$1,131.00

8:48:40 9/26/2018 4761049645711550000 Robert Johnson restaurant
\$1,060.00

8:28:55 9/6/2018 4761049645711550000 Robert Johnson bar \$1,017.00

7:41:59 3/26/2018 30181963913340 Laurie Gibbs coffee shop \$1,009.00

8:51:41 12/14/2018 501879657465 Megan Price pub \$748.00

7:17:21 4/1/2018 4319653513507 Nancy Contreras coffee shop \$100.00

Out of the first top 100 spending, first nine seems to be suspicious.
They occurred in Bar, pub, restaurant and coffeeshop. Morning is either
too late or too early for bars and pubs expenses. The Restaurants
expenses might be by an employee for morning meetings. But still, they
are too high. Many restaurants open after 11.00AM. Detail investigation
may need on them. The card holders need to be informed of the suspicious
transaction and get confirmed either way. The number of the high expense
for the whole day is about 110. This proportional to the 2-hour window
of the morning spending. There too nothing is conclusive.

Robert Johnson , Crystal Clark and Nancy Contreras cards are used
repeatedly, in this type of expenses. But the spending dates are not
close enough to create a suspicion. A hacker may not wait with a stolen
credit to make expenses on it. He/she would like to exploit the card as
soon as possible.

3). The five Merchants prone for small amount hacking are:

a\. Small food vendors

b\. Restaurants & pubs

c\. Dollar Shops

d\. Convenient Stores

e\. bars

