SELECT   
 a.card_num as "Credit Card Number",
 b.card_holder_id as "Card Holder ID",
 c.card_holder_name as "Customer Name",
 count(b.card_holder_id) as "Number of Trasactions",
 sum(a.amount) as "Total Charges to Card"
 

FROM public.transaction as a
INNER JOIN public.credit_card as b ON a.card_num = b.card_num
INNER JOIN card_holder as c ON b.card_holder_id = c.card_holder_id
group by a.card_num,c.card_holder_name,b.card_holder_id
order by b.card_holder_id


  