﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.


CREATE TABLE "card_holder" (
    "card_holder_id" int   NOT NULL,
    "card_holder_name" string   NOT NULL,
    CONSTRAINT "pk_card_holder" PRIMARY KEY (
        "card_holder_id"
     )
);

CREATE TABLE "credit_card" (
    "card_num" string   NOT NULL,
    "card_holder_id" int   NOT NULL,
    CONSTRAINT "pk_credit_card" PRIMARY KEY (
        "card_num"
     )
);

CREATE TABLE "merchant" (
    "merchant_id" int   NOT NULL,
    "merchant_name" string   NOT NULL,
    "merchant_category_id" int   NOT NULL,
    CONSTRAINT "pk_merchant" PRIMARY KEY (
        "merchant_id"
     )
);

CREATE TABLE "merchant_category" (
    "merchant_category_id" int   NOT NULL,
    "merchant_category_name" string   NOT NULL,
    CONSTRAINT "pk_merchant_category" PRIMARY KEY (
        "merchant_category_id"
     )
);

CREATE TABLE "transaction" (
    "transaction_id" int   NOT NULL,
    "transaction_date" datestamp   NOT NULL,
    "amount" number   NOT NULL,
    "card_num" string   NOT NULL,
    "merchant_id" int   NOT NULL,
    CONSTRAINT "pk_transaction" PRIMARY KEY (
        "transaction_id"
     )
);

ALTER TABLE "credit_card" ADD CONSTRAINT "fk_credit_card_card_holder_id" FOREIGN KEY("card_holder_id")
REFERENCES "card_holder" ("card_holder_id");

ALTER TABLE "merchant" ADD CONSTRAINT "fk_merchant_merchant_category_id" FOREIGN KEY("merchant_category_id")
REFERENCES "merchant_category" ("merchant_category_id");

ALTER TABLE "transaction" ADD CONSTRAINT "fk_transaction_card_num" FOREIGN KEY("card_num")
REFERENCES "credit_card" ("card_num");

ALTER TABLE "transaction" ADD CONSTRAINT "fk_transaction_merchant_id" FOREIGN KEY("merchant_id")
REFERENCES "merchant" ("merchant_id");

